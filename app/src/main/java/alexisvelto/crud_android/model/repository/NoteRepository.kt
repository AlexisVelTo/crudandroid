package alexisvelto.crud_android.model.repository

import alexisvelto.crud_android.model.Note
import alexisvelto.crud_android.util.SingletonHolder
import android.content.Context
import com.parse.ParseQuery
import io.reactivex.Observable
import kotlin.collections.ArrayList

class NoteRepository private constructor(context: Context){
    var context = context
    companion object : SingletonHolder<NoteRepository, Context>(::NoteRepository)

    fun getNotes(): Observable<ArrayList<Note>> {
        return Observable.fromCallable {
            var notes = ParseQuery.getQuery<Note>(Note().className).fromLocalDatastore().find()
            return@fromCallable ArrayList(notes)
        }
    }

    fun deleteNote(note:Note){
        note.unpin()
    }

    fun editNote(title:String,content:String,note: Note){
        note.title = title
        note.content = content
        note.pin()
    }

    fun addNote(note: Note){
        note.pin() //Save the note in the local datastore
    }
}