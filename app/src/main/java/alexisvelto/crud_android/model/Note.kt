package alexisvelto.crud_android.model

import alexisvelto.crud_android.util.ParseDelegate
import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Note")
class Note: ParseObject() {
    var title by ParseDelegate<String?>()
    var content by ParseDelegate<String?>()
}