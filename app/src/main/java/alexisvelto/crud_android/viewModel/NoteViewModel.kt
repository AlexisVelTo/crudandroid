package alexisvelto.crud_android.viewModel

import alexisvelto.crud_android.model.Note
import alexisvelto.crud_android.model.repository.NoteRepository
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class NoteViewModel (application: Application): AndroidViewModel(application) {
    val noteRepository = NoteRepository.getInstance(application.applicationContext)
    val mNotes = MutableLiveData<ArrayList<Note>>()
    var isLoading = MutableLiveData<Boolean>()

    fun loadNotes(){
        isLoading.value = true
        noteRepository.getNotes().subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object: DisposableObserver<ArrayList<Note>>(){
                override fun onComplete() {
                    isLoading.value = false
                }
                override fun onNext(notes: ArrayList<Note>) {
                    mNotes.value = notes
                }
                override fun onError(e: Throwable) {

                }
            })
    }

    fun addNote(note:Note){
        noteRepository.addNote(note)
    }

    fun deleteNote(note:Note){
        noteRepository.deleteNote(note)
    }

    fun edit(title:String,content:String,note: Note){
        noteRepository.editNote(title,content,note)
    }

}