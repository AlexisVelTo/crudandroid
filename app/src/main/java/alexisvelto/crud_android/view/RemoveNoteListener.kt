package alexisvelto.crud_android.view

import alexisvelto.crud_android.model.Note

interface RemoveNoteListener {
    fun delete(note:Note)
    fun edit(title:String,content:String,note:Note)
}