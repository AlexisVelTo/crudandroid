package alexisvelto.crud_android.view

import alexisvelto.crud_android.R
import alexisvelto.crud_android.model.Note
import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton


class NoteAdapter(listener: RemoveNoteListener) : RecyclerView.Adapter<NoteAdapter.NoteViewHolder>(){
    private var noteList = ArrayList<Note>()
    private var lister = listener

    fun replaceData(newData: ArrayList<Note>){
        noteList = newData
        notifyDataSetChanged()
    }

    fun addNote(note:Note){
        noteList.add(note)
        notifyDataSetChanged()
    }
    fun removeAt(position: Int) {
        noteList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, noteList.size)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteAdapter.NoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return NoteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    override fun onBindViewHolder(holder:NoteAdapter.NoteViewHolder, position: Int) {
        val note = noteList[position]
        holder.noteTitle.text = note.title
        holder.noteContent.text = note.content
        holder.deleteItem.setOnClickListener {
            lister.delete(note)
            removeAt(position)
        }

        holder.noteItem.setOnClickListener {
            var myDialog = Dialog(holder.itemView.context)
            myDialog.setContentView(R.layout.dialog_add_note)
            var dialogTitle = myDialog.findViewById<TextView>(R.id.textview_dialog_title)
            dialogTitle.setText(holder.itemView.context.getString(R.string.dialog_title_update))
            var button = myDialog.findViewById<MaterialButton>(R.id.button_confirm)
            button.setText(holder.itemView.context.getString(R.string.button_edit_note))
            var editTextTitle = myDialog.findViewById<EditText>(R.id.edittext_title)
            editTextTitle.setText(note.title)
            var editTextContent = myDialog.findViewById<EditText>(R.id.edittext_content)
            editTextContent.setText(note.content)
            var buttonCancel = myDialog.findViewById<MaterialButton>(R.id.button_cancel)
            buttonCancel.setOnClickListener {
                myDialog.dismiss()
            }
            button.setOnClickListener {
                lister.edit(editTextTitle.text.toString(),editTextContent.text.toString(),note)
                notifyDataSetChanged()
                myDialog.dismiss()
            }

            myDialog.show()
        }
    }

    class NoteViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var noteItem: CardView = itemView.findViewById(R.id.cardView_item)
        var noteTitle: TextView = itemView.findViewById(R.id.textView_title)
        var noteContent: TextView = itemView.findViewById(R.id.textView_content)
        var deleteItem:ConstraintLayout = itemView.findViewById(R.id.layout_delete_note)
    }
}