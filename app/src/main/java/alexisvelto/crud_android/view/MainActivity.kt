package alexisvelto.crud_android.view

import alexisvelto.crud_android.R
import alexisvelto.crud_android.model.Note
import alexisvelto.crud_android.viewModel.NoteViewModel
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import android.widget.ViewSwitcher
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),RemoveNoteListener {


    lateinit var mainViewModel:NoteViewModel
    lateinit var  adapter:NoteAdapter
    lateinit var viewSwitcher: ViewSwitcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = NoteAdapter(this)
        viewSwitcher = findViewById(R.id.view_switcher)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview_notes)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        mainViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)
        mainViewModel.mNotes.observe(this, Observer {
            if (it.isEmpty()){
                if (R.id.layout_no_data === viewSwitcher.nextView.id) {
                    viewSwitcher.showNext()
                }
            } else {
                if (R.id.recyclerview_notes === viewSwitcher.nextView.id) {
                    viewSwitcher.showNext()
                }
            }
            adapter.replaceData(it)
        })

        button_add_note.setOnClickListener {
            var myDialog = Dialog(this)
            myDialog.setContentView(R.layout.dialog_add_note)
            var dialogTitle = myDialog.findViewById<TextView>(R.id.textview_dialog_title)
            dialogTitle.setText(getString(R.string.dialog_title_add))
            var buttonAdd = myDialog.findViewById<MaterialButton>(R.id.button_confirm)
            buttonAdd.setText(R.string.button_add_note)
            var editTextTitle = myDialog.findViewById<EditText>(R.id.edittext_title)
            var editTextContent = myDialog.findViewById<EditText>(R.id.edittext_content)
            var buttonCancel = myDialog.findViewById<MaterialButton>(R.id.button_cancel)
            buttonCancel.setOnClickListener {
                myDialog.dismiss()
            }
            buttonAdd.setOnClickListener {
                if(editTextTitle.text.isBlank() || editTextContent.text.isBlank()){
                    Toast.makeText(this, getString(R.string.toast_fill_data), Toast.LENGTH_LONG).show()
                }else{
                    var note = Note()
                    note.title = editTextTitle.text.toString()
                    note.content = editTextContent.text.toString()

                    mainViewModel.addNote(note)
                    adapter.addNote(note)
                    mainViewModel.loadNotes()
                    myDialog.dismiss()
                }

            }

            myDialog.show()
        }
    }
    override fun delete(note: Note) {
        mainViewModel.deleteNote(note)
        mainViewModel.loadNotes()
    }
    override fun edit(title:String,content:String,note: Note) {
        mainViewModel.edit(title,content,note)
        mainViewModel.loadNotes()
    }
    override fun onResume() {
        super.onResume()
        mainViewModel.loadNotes()
    }
}
