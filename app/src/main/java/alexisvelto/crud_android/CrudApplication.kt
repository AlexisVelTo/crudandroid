package alexisvelto.crud_android

import alexisvelto.crud_android.model.Note
import android.app.Application
import com.parse.Parse
import com.parse.ParseObject

class CrudApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        ParseObject.registerSubclass(Note::class.java)
        Parse.initialize(
            Parse.Configuration.Builder(this)
                .applicationId("cKuj6RMs3KcD6UiSEllcGG0kQbz1JqyALa5lgjjz")
                .clientKey("9qoG0IT3E2p1K82yfP33qXcLMvLgeiwAU3HqsNE7")
                .server("https://parseapi.back4app.com/")
                .enableLocalDataStore()
                .build())
    }
}